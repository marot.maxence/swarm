#include <QApplication>
#include <QtGui>
#include "mainwindow.h"

// Only launch the app
int main(int argc, char * argv[])
{
    QApplication app(argc, argv);

    MainWindow window;

    return app.exec();
}


/// Gitlab repository for those wanting potential updates: https://gitlab.com/marot.maxence/swarm
