#ifndef ENTITY_H
#define ENTITY_H

#include <QtWidgets>
#include <math.h>

//  World constants (hardcoded, need to be dynamic, but not of high priority)
#define PI 3.14159265

#define maxSpeed 3
#define DrawingTime 50

// Not definitive, radius in which we take our friends when we think
#define friendRadius 55

class Entity : public QGraphicsObject
{
    // Needed to use personalized slots
    Q_OBJECT

  public:

    // Constructor
        Entity(int coordX, int coordY);

    // Getters
        float getSpeedX() const;
        float getSpeedY() const;
        bool isInfected() const;
        bool isAlive() const;
        bool isInCemetery() const;
        bool isCarried() const;
        bool knowCemeteryLocation() const;

    // Setters
        void infect(bool infect);
        void carry(float vx, float vy);
        void isCarried(bool carried);
        void setColor(QColor color);
        void isInCemetery(bool in);
        void knowCemeteryLocation(bool knowCemeteryLocation);

    // Methods
        float magnitude();

    // Static methods
        static bool getIfDirection();
        static void toggleIfDirection(bool direction);

        static bool getIfCohesion();
        static void toggleIfCohesion(bool cohesion);

        static bool getIfAvoidFriends();
        static void toggleIfAvoidFriends(bool avoidFriends);

        static bool getIfWander();
        static void toggleIfWander(bool wander);

protected:
        // Methods
        void advance(int phase);
        QRectF boundingRect() const;
        QRectF actualRect() const;
        void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);

        QVariant itemChange(GraphicsItemChange change, const QVariant &value);

  private:
    // Attributes
        //! \variable The angle of the direction the Entity is heading to.
        int _angle;
        //! The width of the bounding rectangle of the Entity.
        int _width;
        //! The height of the bounding rectangle of the Entity.
        int _height;
        //! Color of the Entity.
        QColor _color;
        //! List containing all nearby Entities.
        QList<Entity *> _friends;
        //! Pointer to an Enity if this Entity carry a dead.
        Entity * _carried;

        //! Speed of the Entity horizontally (negative goes to the left, positive goes to the right).
        float _speedX;
        //! Speed of the Entity vertically (negative goes to the top, positive goes to the bottom).
        float _speedY;

        //! Time the Entity takes to change of friends.
        int _thinkTimer;
        //! Time the Entity needs to change color once.
        float _blinktime;
        //! Used with _blinktime to change color.
        QTimer * _timerBrush;
        //! \c true if the Entity is infected or \c false otherwise.
        bool _isInfected;
        //! \c true if the Entity tries not to spread the infection or \c false otherwise.
        bool _careful;
        //! Represent the risk of dying from the infection.
        int _risky;
        //! \c true if the Entity is alive or \c false if it has died due to the infection.
        bool _alive;
        //! This attribute is used only when the Entity is dead \c true if the Entity is in a cemetary or \c false otherwise.
        bool _isInCemetery;
        //! This attribute is used to determine if the Entity knows where is the cemetery located.
        //!  \c true if the Entity knows where the cemetery is or \c false otherwise.
        bool _knowCemeteryLocation;

        //! \c true if the Entity carries a dead Entity or \c false otherwise.
        bool _carry;
        //! \c true if the dead Entity is carried by another Entity or \c false otherwise.
        bool _isCarried;

        //! If \c true -> activate getAverageDir(), if \c false, deactivate
        static bool _direction;
        //! If \c true -> activate getCohesion(), if \c false, deactivate
        static bool _cohesion;
        //! If \c true -> activate getAvoidFriends(), if \c false, deactivate
        static bool _avoidFriends;
        //! If \c true -> activate wander(), if \c false, deactivate
        static bool _wander;
        //! If \c true -> there is a cemetery, \c false if not
        static bool _cemetery;
        //! X position of the cemetery
        static int _cemeteryX;
        //! Y position of the cemetery
        static int _cemeteryY;

    // Methods
        float calcAngle(float speedX, float speedY);
        void initWay();

        // Affecting the behaviour of the Entity
        void getFriends();
        void behaviour();
        void infectUpdate();
        void mourn();
        void carry(Entity * carried, int vx, int vy);
        void drop();

        // Affecting the movement of the Entity
        void getAverageDir();
        void getCohesion();
        void getAvoidDir();
        void wander();
        void limit();
        void avoidInfected();

        void checkCemetery();

        // Affecting the color of the Entity
        void blink();


    private slots:
        void resetBrush();
        void continuation();
};

#endif // ENTITY_H
