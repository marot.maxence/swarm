#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QtWidgets>
#include "entity.h"

// Time between each refresh of the window.
#define refreshTime 70

class MainWindow : public QGraphicsScene
{
    public:
        // Constructor
        MainWindow();

        // Methods
        void mousePressEvent(QGraphicsSceneMouseEvent * e);
        void keyPressEvent(QKeyEvent *event);
        bool eventFilter(QObject *watched, QEvent *event);

    public slots:
        void advance();

    private:
        // Attributes
        //! View of the project, is contained in the scene and paints the Entities.
        QGraphicsView * _view;

        //! bool used in mousePressEvent to add an infected Entity if _infectMdoe is \c true, or a non infected one if _infectMode is \c false
        bool _infectMode;
};

#endif // MAINWINDOW_H
