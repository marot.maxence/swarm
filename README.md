# Swarm

Project summary (as written in the entity.cpp file, also here to tell how to use the program)

You can add Entities on the scene by left clicking on the window, the Entities will then behave by themselves.

    There are a few ways to modify the evolution of an Entity on the scene:
     - Modifying defined constants such as maxSpeed and friendRadius (can only be hardcoded atm).
     - Modifying the advance(int phase) method to change how the Entity will change its position.
     - Modifying the behaviour() method to change how the Entity interacts with others.
    

    To change the group behaviour of the Entities, you will need to use the numeric keypad:

     - Press 1 to switch on or off the following of the average direction of the group, on by default
     - Press 2 to switch on or off the cohesion of the group (the Entities will try to stick to each other), on by default
     - Press 3 to switch on or off the crowd avoidance, as to not have Entities colliding (too much), on by default
     - Press 4 to switch on or off the wandering trait of Entities, to make them change direction a little at random, on by default
    

    Last but not least, Entities can spread an infection.
    Pressing 5 on the numeric keyboard will allow you to add switch between infected and normal Entities when left clicking, normal by default.
    The infection will spread according to some probabilities, which can be modified to have a more virulent infection (hardcoded for now).
    After some time, the infected Entities will ether recover or die:
      - if they recover, they will behave normally.
      - if they die, they will become a corpse and stay in place.
    Living Entities can carry corpses and drop them in a cemetery.
    The cemetery is the place where the first Entity that dies is.
    When an Entity know the location of the cemetery, it will tell others and if they carry a corpse, they will go to the cemetery.

    I hope you will enjoy our project as much as we enjoyed doing it !

    With too much Entities, framerate will drop after a certain time, sorry for the inconvenience.
    Also, static Entities have their colors change when other Entities comes nearby, don't know how to correct it.

Tâches:

- Done:
  - Afficher une fenêtre
  - Créer une classe (hérite de QGraphicsItem ?) pour afficher une entité dans la fenêtre (paint sert à l'affichage)
  - Faire en sorte que l'entité se déplace (méthode advance done avec un signal de QTimer pour répéter toutes les x millisecondes)
  - Gérer la sortie des entitées du périmètre de la fenêtre (j'ai enlevé les rebonds contre les bords, je les fait sauter de l'autre côté de l'écran)
  - Faire en sorte que les entitées se déplacent aléatoirement à la création
  - Faire en sorte qu'on puisse rajouter une entité avec un click
  - changer l'orientation du triangle en fonction de la direction qu'elle prend (ajout de calcAngle)
  - gérer le changement de taille de la fenêtre

  - Créer une méthode comportement dans entité (on essaie de mettre tous les noms en anglais pour que ce soit propre oc) #DONE
    - Faire en sorte qu'elles se déplacent au pif après un temps au pif ? #DONE -> Wander
      - Voir si on fait une méthode comportementSolo et comportementGroupe du coup (gérer le cas où une entité est seule) # géré en vérifiant si elle a des amis (entitées proches)
    - Pour gérer le comportement en groupe, on doit pouvoir avoir la position des voisins et la direction des voisins (minimum, rajouter des méthodes qui gèrent ça) #DONE

    - Faire en sorte d'avoir plusieurs mode au clic ou en appuyant sur un bouton spécifique # DONE
      - En appuyant genre sur ctrl ça change ton mode, genre tu peux rajouter des obstacles (Classe à ajouter je suppose, QGraphicsItem aussi), changer entre plusieurs comportements prédéfinis ...
		(# DONE 1 pour direction, 2 pour cohesion, 3 pour évitement, 4 pour vagabondage)

    - Réparer les bugs qui modifient les directions (bugs dans mes calculs certainement)

    - Corrections mineures sur les déplacements (qui ne sont forcément pas parfaits) et les clignotements (idem)

  - Comportement de l'infection                                                                    #DONE
     - Ajouter un attribut (bool) qui dit si une Entité est infectée                               #DONE
     - Ajouter une méthode qui détermine quoi faire si une Entité est infectée (distanciation)     #DONE
     - Temps d'incubation de l'infection ? -> nécessite un lockdown
     - Tuer les entités infectées au bout d'un certain temps pour voir l'évolution de l'infection au fil du temps
     - Donner des comportements aléatoires aux entités 
        (si infectée, s'isole ou non, si non infecté, évite les infectés plus ou moins #antivax, ou un taux de respect des gestes barrière diminue le risque d'infection)
     - Mise en place de cimetières pour déposer les corps.
     - Les entités vivantes ramassent maintenant les corps et se déplacent vers le cimetière si elles en connaissent la position.

- TODO: 
  - Faire en sorte qu'on puisse changer la vitesse max ainsi que la distance à laquelle on choisi nos voisins en fonction de la taille de la fenêtre

  - Faire plusieurs champs d'action - Les voisins les plus proches influent plus que les voisins plus lointains #DONE pour l'infection

  - Ajouter un visuel pour savoir quelle comportement on active/désactive

- Idées d'ajouts:
  - prédateurs
  - créer des obstacles
  - donner un objectif aux entités, plus une entité est proche plus elle envoie un signal fort aux autres, l'objectif étant que le plus d'entités possibles atteignent l'objectif
    (avec des obstacles sur la route forcément)
  - Principe d'évolution, chaque entité aura des caractéristique propres, et celles qui auront accès le plus facilement à la nourriture se reproduiraient.
      -> La descendance serait donc de plus en plus efficace pour accéder à la nourriture.


Informations complémentaires: 

Le stage est fini, et nonobstant le fait que j'ai d'autres projets perso à faire, j'y reviendrai sûrement, il était super intéressant.
// -> J'apprends à utiliser Unity, d'ici une semaine ou deux je reprendrais sans doute ic pour changer et j'essaierai de rajouter des fonctionnalités/ rendre un peu plus propre le tout.