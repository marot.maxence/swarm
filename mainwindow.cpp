/*!
    \file mainwindow.cpp
    \brief Class representing the window in which everything takes place.

    \author Maxence Marot & Nicolas Saunier
    \date 04/05/2020

*/

/*!
    \class MainWindow
    \brief The MainWindow class is the window of the project.
           It is here that we intercept every event comming from the user.

    The MainWindow class inherits QGraphicsScene and not directly QWidget because we didn't need to add a lot to the main window.

    From here, you can add event handlers in three ways:

    \list
    \li Modifying mousePressEvent(QGraphicsSceneMouseEvent *e) to add a functionality when you press a button of the mouse.
        At the moment, the left click is used to add an Entity to the scene.
    \li Modifying keyPressEvent(QKeyEvent *event) to add a functionality when you press a button of the keyboard.
        At the moment, keys 1, 2, 3, 4 are used to activate or deactivate specific behaviours of the Entities.
    \li Modifying eventFilter(QObject *watched, QEvent *event) to handle other events.
        At the moment, it is used to manage the changement in size of the window.
    \endlist

*/

#include "mainwindow.h"

/*!
 * \brief Constructor of MainWindow.
          Define the needed parameters and execute the program.
*/
MainWindow::MainWindow()
{
    _view = new QGraphicsView(this);
    _view->resize(1024, 576);
    // Put the coordinates (0;0) on the top left hand corner
    _view->setAlignment(Qt::AlignLeft | Qt::AlignTop);

    this->setSceneRect(_view->rect());
    _view->setWindowTitle("SWARM");

    // remove scrollbars
    _view->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    _view->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    _view->setBackgroundBrush(Qt::darkGray);

    // Timer used to triger the advance of the entities we will put in the scene
    QTimer *timer = new QTimer(this);
    QObject::connect(timer, SIGNAL(timeout()), this, SLOT(advance()));

    // Time to wait before each call of advance SLOT (in milliseconds)
    timer->start(refreshTime);

    // Install eventFilter (see eventFilter method of MainWindow)
    _view->installEventFilter(this);

    // By default, infect mode is not toggled
    _infectMode = false;

    _view->show();
}

/*!
    \reimp
    \fn void MainWindow::mousePressEvent(QGraphicsSceneMouseEvent *e)
    \param e the mouse event to handle.

    \brief Method called with each click on the mouse.
           At the moment, the left click is used to add an Entity to the scene.

    \note To avoid clicking like a starcraft player micromanaging to get big groups, need to add a way to add several Entities at once.
*/
void MainWindow::mousePressEvent(QGraphicsSceneMouseEvent *e)
{
    // If the left button of the mouse is pressed, we summon an entity and add it to the scene
    if(e->button() == Qt::LeftButton)
    {
        if(!_infectMode)
        {
            Entity * ent = new Entity(e->scenePos().x(), e->scenePos().y());

            this->addItem(ent);
        }
        else
        {
            Entity * ent = new Entity(e->scenePos().x(), e->scenePos().y());
            ent->infect(true);
            this->addItem(ent);
        }
    }
    // In case we need the right button ?
    /*else
    {

    }*/
}

/*!
    \reimp
    \fn void MainWindow::keyPressEvent(QKeyEvent *event)
    \param event the keyboard event to handle.

    \brief Method called with each key pressed on the keyboard.
           At the moment, keys 1, 2, 3, 4 are used to activate or deactivate specific behaviours of the Entities.
           5 is used to toggle the infect mode.

    \note Need to add a way to see which behaviour is toggled
 */
void MainWindow::keyPressEvent(QKeyEvent *event)
{
    if(event->key() == Qt::Key_1)
    {
        Entity::toggleIfDirection(!Entity::getIfDirection());
    }
    if(event->key() == Qt::Key_2)
    {
        Entity::toggleIfCohesion(!Entity::getIfCohesion());
    }
    if(event->key() == Qt::Key_3)
    {
        Entity::toggleIfAvoidFriends(!Entity::getIfAvoidFriends());
    }
    if(event->key() == Qt::Key_4)
    {
        Entity::toggleIfWander(!Entity::getIfWander());
    }
    if(event->key() == Qt::Key_5)
    {
        _infectMode = !_infectMode;
    }
}

/*!
    \reimp
    \fn bool MainWindow::eventFilter(QObject *watched, QEvent *event)
    \param watched the object which receives the event.
    \param event the event to handle.

    \brief Method used to filter and use events that passes through our MainWindow.
           At the moment, it is used to manage the changement in size of the window.

    \return a \c boolean, \c true if the event was handled, \c false otherwise.
 */
bool MainWindow::eventFilter(QObject *watched, QEvent *event)
{
    // On the resize of the window, refresh the bounding rectangle of the scene
    // to have the right size
    if(event->type() == QEvent::Resize)
    {
        setSceneRect(QRectF());
        setSceneRect(0, 0, _view->width(), _view->height());
    }
    // Return false for any other events (otherwise it will block the events used for the entities)
    else
    {
        return false;
    }

    return true;
}

/*!
    \reimp
    \fn void MainWindow::advance()
    \brief Call each the advance() method for each item in the scene.

    \sa QTimer, Entity::advance()
 */
void MainWindow::advance()
{
    for (int i = 0; i < 2; ++i) {
        const auto items_ = items();
        for (QGraphicsItem *item : items_)
        {
            item->advance(i);
            update();
        }
    }
}
