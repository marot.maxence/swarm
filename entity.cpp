/*!
    \file entity.cpp
    \brief Class representing an Entity which goal is to adapt its behaviour depending on its direct neighbours behaviours.
           It is the main part of our project.

    \author Maxence Marot & Nicolas Saunier
    \date 04/05/2020

*/

/*!
    \class Entity
    \brief The Entity class is the main object of the project.
           Its main goal is to interact with other Entities to form a kind of swarm intelligence.

    The Entity class inherits QGraphicsObject to use inherited methods from both
    QObject and  QGraphicsItem.

    You can add Entities on the scene by left clicking on the window, the Entities will then behave by themselves.

    There are a few ways to modify the evolution of an Entity on the scene:

    \list
    \li Modifying defined constants such as \c maxSpeed and \c friendRadius (can only be hardcoded atm).
    \li Modifying the \c advance(int phase) method to change how the Entity will change its position.
    \li Modifying the \c behaviour() method to change how the Entity interacts with others.
    \endlist

    To change the group behaviour of the Entities, you will need to use the numeric keypad:

    \list
    \li Press 1 to switch on or off the following of the average direction of the group, on by default
    \li Press 2 to switch on or off the cohesion of the group (the Entities will try to stick to each other), on by default
    \li Press 3 to switch on or off the crowd avoidance, as to not have Entities colliding (too much), on by default
    \li Press 4 to switch on or off the wandering trait of Entities, to make them change direction a little at random, on by default
    \endlist

    Last but not least, Entities can spread an infection.
    Pressing 5 on the numeric keyboard will allow you to add switch between infected and normal Entities when left clicking, normal by default.
    The infection will spread according to some probabilities, which can be modified to have a more virulent infection (hardcoded for now).
    After some time, the infected Entities will ether recover or die:
      - if they recover, they will behave normally.
      - if they die, they will become a corpse and stay in place.
    Living Entities can carry corpses and drop them in a cemetery.
    The cemetery is the place where the first Entity that dies is.
    When an Entity know the location of the cemetery, it will tell others and if they carry a corpse, they will go to the cemetery.

    I hope you will enjoy our project as much as we enjoyed doing it !

    \note With too much Entities, framerate will drop after a certain time, sorry for the inconvenience.
          Also, static Entities have their colors change when other Entities comes nearby, don't know how to correct it.
*/

#include "entity.h"
#include "iostream"
#include <math.h>

// Static attributes
// (if true, related behaviour is on)
bool Entity::_direction = true;
bool Entity::_cohesion = true;
bool Entity::_avoidFriends = true;
bool Entity::_wander = true;

// Static attributes used to manage the cemetery and the deposit of the corpses
bool Entity::_cemetery = false;
int Entity::_cemeteryX = 0;
int Entity::_cemeteryY = 0;

/*!
    \brief Constructor of Entity.
           Constructs an Entity at the given coordinates.

    \param coordX the starting X coordinate.
    \param coordY the starting Y coordinate.

    \sa QGraphicsItem::setX(qreal x), QGraphicsItem::setY(qreal y)
*/
Entity::Entity(int coordX, int coordY)
{
    // Allow the itemChange function to trigger (might remove later)
    setFlag(QGraphicsItem::ItemSendsScenePositionChanges);

    // Setting starting position
    setX(coordX);
    setY(coordY);

    // At the moment, by default, width & height of the entity are 20 (fixed)
    // (might make it variable later depending on the size of the window)
    _width = 20;
    _height = 20;

    // Pseudo-Random number generator
    // seed is the number of milliseconds since the Unix epoch (1970 00:00:00)
    qsrand(QDateTime::currentMSecsSinceEpoch());

    // Choose the direction of the entity
    initWay();

    // See advance() for proper use and explanations
    _thinkTimer = qrand()%10;


    /* Set the angle the Entity point to
     *
     *   Piece of information regarding the angle
     *
     * 0   point out to the top left hand corner == 360
     * 45  point out to the top
     * 90  point out to the top right hand corner
     * 135 point out to the right
     * 180 point out to the bottom right hand corner
     * 225 point out to the bottom
     * 270 point out to the bottom left hand corner
     * 315 point out to the left
     * 360 point out to the top left hand corner == 0
    */
    _angle = calcAngle(_speedX, _speedY);
    setRotation(_angle);

    // Timer used to trigger the resetBrush method of the entity
    _timerBrush = new QTimer(this);
    QObject::connect(_timerBrush, SIGNAL(timeout()), this, SLOT(resetBrush()));

    _blinktime = (qrand()%100+50)*1000;
    _timerBrush->start(_blinktime);

    // Initializing useful attributes (see doc for more explanations)
    _isInfected = false;

    _careful = (qrand()%100 < 90) ? true : false;
    _risky = qrand()%100;

    _alive = true;
    _isInCemetery = false;

    _carry = false;
    _isCarried = false;

    _carried = nullptr;

    _knowCemeteryLocation = false;
}

// Public Getters
/*!
    \fn float Entity::getSpeedX() const
    \brief Getter of _speedX.

    \return the speed of the Entity horizontally: positive goes to the right, negative goes to the left
*/
float Entity::getSpeedX() const
{
    return _speedX;
}

/*!
    \fn float Entity::getSpeedY() const
    \brief Getter of _speedY.

    \return the speed of the Entity vertically: positive goes to the bottom, negative goes to the top
*/
float Entity::getSpeedY() const
{
    return _speedY;
}

/*!
    \fn bool Entity::isInfected() const
    \brief Getter of _isInfected

    \return the state of the infection for the Entity: \c true if infected, \c false otherwise
*/
bool Entity::isInfected() const
{
    return _isInfected;
}

/*!
    \fn bool Entity::isAlive() const
    \brief Getter of _alive

    \return the state of the Entity: \c true if alive, \c false if as died from the infection
*/
bool Entity::isAlive() const
{
    return _alive;
}

/*!
    \fn bool Entity::isInCemetery() const
    \brief Getter of _isInCemetery

    \return the state of the dead Entity: \c true if in cemetery, \c false if it is randomly placed on the window

    \sa checkCemetery()
*/
bool Entity::isInCemetery() const
{
    return _isInCemetery;
}

/*!
    \fn bool Entity::isCarried() const
    \brief Getter of _carried

    \return the state of the dead Entity: \c true if carried, \c false if still

    \sa carried(float vx, float vy), carried(Entity * carried, float vx, float vy)
*/
bool Entity::isCarried() const
{
    return _isCarried;
}

/*!
    \fn bool Entity::knowCemeteryLocation() const
    \brief Getter of _knowCemeteryLocation

    \return \c true if the Entity knows where the cemetery is, \c false otherwise

    \sa knowCemeteryLocation(bool knowCemeteryLocation)
*/
bool Entity::knowCemeteryLocation() const
{
    return _knowCemeteryLocation;
}

// Public Setters
/*!
    \fn void Entity::infect(bool infect)
    \param infect, a boolean that is \c true if the Entity is infected, \c false otherwise

    \brief Setter of _isInfected.
           If an Entity becomes infected, change its color, trigger the continuation() slot and drop any carried Entity.

    \sa continuation(), drop(), isInfected()
 */
void Entity::infect(bool infect)
{
    if(infect)
    {
        // If this Entity cary a corpse, drop it
        if(_carry)
        {
            drop();
        }

        // Stop the blinking and change the color to green
        _timerBrush->stop();
        _color = Qt::darkGreen;
        _isInfected = infect;

        // Trigger the continuation slot after a random time included between 10k & 30k milliseconds
        QTimer * t = new QTimer(this);
        t->singleShot(qrand()%20000+10000, this, SLOT(continuation()));
    }
    else
    {
        _isInfected = infect;
    }
}

/*!
    \fn void Entity::carry(float vx, float vy)
    \brief change the speed of this Entity (if dead and carried)

    \sa drop(), carried(Entity * carried, float vx, float vy)
*/
void Entity::carry(float vx, float vy)
{
    _speedX = vx;
    _speedY = vy;
}

/*!
    \fn void Entity::isCarried(bool carried)
    \brief Setter of _isCarried
*/
void Entity::isCarried(bool carried)
{
    _isCarried = carried;
}

/*!
    \fn void Entity::setColor(QColor color)
    \brief Setter of _color
*/
void Entity::setColor(QColor color)
{
    _color = color;
}

/*!
    \fn void Entity::isInCemetery(bool in)
    \brief Setter of _isInCemetery
*/
void Entity::isInCemetery(bool in)
{
    _isInCemetery = in;
}

/*!
    \fn void Entity::knowCemeteryLocation(bool knowCemeteryLocation)
    \brief Setter of _knowCemeteryLocation

    \sa knowCemeteryLocation()
*/
void Entity::knowCemeteryLocation(bool knowCemeteryLocation)
{
    _knowCemeteryLocation = knowCemeteryLocation;
}

// Public methods
/*!
    \fn float Entity::magnitude()
    \brief Calculate and return the magnitude of the speed vector of the Entity.

    \note Might cause some perfomance issues when called too often (sqrt).

    \return the magnitude of the speed vector of the Entity
*/
float Entity::magnitude()
{
    return sqrt(_speedX*_speedX+_speedY*_speedY);
}

// Public static Getters & Setters
/*!
    \fn bool Entity::getIfDirection()
    \brief Getter of _direction.

    \return the value of static _direction

    \sa toggleIfDirection(bool direction), getAverageDir()
*/
bool Entity::getIfDirection()
{
    return _direction;
}

/*!
    \fn void Entity::toggleIfDirection(bool direction)
    \param direction

    \brief Set the value of static _direction attribute which represent the activation of the getAverageDir() method.
    \c true to activate.
    \c false to deactivate

    \sa getIfDirection(), getAverageDir()
 */
void Entity::toggleIfDirection(bool direction)
{
    _direction = direction;
}

/*!
    \fn bool Entity::getIfCohesion()
    \brief Getter of _cohesion.

    \return the value of static _cohesion attribute which represent the activation of the getCohesion() method.

    \sa toggleIfCohesion(bool cohesion), getCohesion()
*/
bool Entity::getIfCohesion()
{
    return _cohesion;
}

/*!
    \fn void Entity::toggleIfCohesion(bool cohesion)
    \param cohesion

    \brief Set the value of static _cohesion attribute which represent the activation of the getCohesion() method.
    \c true to activate.
    \c false to deactivate

    \sa getIfCohesion(), getCohesion()
*/
void Entity::toggleIfCohesion(bool cohesion)
{
    _cohesion = cohesion;
}

/*!
    \fn bool Entity::getIfAvoidFriends()
    \brief Getter of _avoidFriends.

    \return the value of static _avoidFriends attribute which represent the activation of the getAvoidDir() method.

    \sa toggleIfAvoidFriends(bool avoidFriends), getAvoidDir()
*/
bool Entity::getIfAvoidFriends()
{
    return _avoidFriends;
}

/*!
    \fn void Entity::toggleIfAvoidFriends(bool avoidFriends)
    \param avoidFriends

    \brief Set the value of static _avoidFriends attribute which represent the activation of the getAvoidDir() method.
    \c true to activate.
    \c false to deactivate

    \sa getIfAvoidFriends(), getAvoidDir()
*/
void Entity::toggleIfAvoidFriends(bool avoidFriends)
{
    _avoidFriends = avoidFriends;
}

/*!
    \fn bool Entity::getIfWander()
    \brief Getter of _wander.

    \return the value of static _wander attribute which represent the activation of the wander() method.

    \sa toggleIfWander(bool wander), wander()
*/
bool Entity::getIfWander()
{
    return _wander;
}

/*!
    \fn void Entity::toggleIfWander(bool wander)
    \param wander

    \brief Set the value of static _wander attribute which represent the activation of the wander() method.
    \c true to activate.
    \c false to deactivate

    \sa getIfWander(), wander()
*/
void Entity::toggleIfWander(bool wander)
{
    _wander = wander;
}

// Protected methods

/*!
    \reimp
    \fn void Entity::advance(int phase)
    \param phase is used internally by Qt

    \brief Make the Entity move and calls the getFriends() & behaviour() methods.
           Is also used to place the cemetery with a call to checkCemetery();
    \note Is called every x milliseconds where x is the time defined in MainWindow.

    \sa MainWindow, behaviour(), QTimer, checkCemetery()
*/
void Entity::advance(int phase)
{
    // Set the coordinates to the right ones on the scene
    setX(fmod((x()+scene()->width()),scene()->width()));
    setY(fmod((y()+scene()->height()),scene()->height()));

    if(_alive)
    {

        // Modify the value of thinkTimer, used to search for friends nearby
        _thinkTimer = (_thinkTimer + 1)%5;

        if(_thinkTimer == 0)
        {
            // Calls the getFriends() method which search for friends nearby
            // to affect the behaviour of this Entity.
            getFriends();
        }

        // Calls the behaviour() method which change the speed vector of this Entity
        // depending on its neighbours
        behaviour();

        _angle = calcAngle(_speedX, _speedY);
        setRotation(_angle);
    }
    else if(!Entity::_cemetery)
    {
        checkCemetery();
    }

    // Set the new position
    setPos(x()+_speedX, y()+_speedY);


    // Test if the entity is within the boundaries of the window
    // If not, move it to the other side of the window
    if(!scene()->sceneRect().contains(actualRect()))
    {
        if(y() < 0)
        {
            setY(scene()->height()-_height/2);
        }
        else if(y() > scene()->height())
        {
            setY(0+_height/2);
        }
        else if(x() < 0)
        {
            setX(scene()->width()-_width/2);
        }
        else if(x() > scene()->width())
        {
            setX(0+_width/2);
        }
    }
}

/*!
    \reimp
    \fn QRectF Entity::boundingRect() const
    \brief The bounding Rectangle of the entity, used to test colliding by Qt.
           It doesn't correspond to the real hitbox as the coordinates are 0;0.

    \return a Rectangle of the size of the Entity.

    \sa actualRect()
*/
QRectF Entity::boundingRect() const
{
    return QRect(0, 0, _width, _height);
}

/*!
    \fn QRectF Entity::actualRect() const
    \brief Getter for the hitbox of the Entity.
           Unlike boundingRect() method, actualRect() returns the bounding rectangle at the coordinates of the Entity.

    \return a Rectangle of the size of the Entity and at the coordinates of the Entity.

    \sa boundingRect()
 */
QRectF Entity::actualRect() const
{
    return QRect(x(), y(), _width, _height);
}

/*!
    \reimp
    \fn void Entity::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
    \param painter
    \param option
    \param widget

    \brief Used to draw the entity on the scene
*/
void Entity::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    // Color of the entity
    painter->setBrush(_color);
    QPolygon polygon;

    //To make a good looking triangle
    polygon << QPoint(0, 0) << QPoint(_width/2, _height) << QPoint(_width, _height/2);
    painter->drawPolygon(polygon);
}

/*!
    \reimp
    \fn QVariant Entity::itemChange(QGraphicsItem::GraphicsItemChange change, const QVariant &value)
    \param change
    \param value

    \brief Used to infect other entities, mourn (drop or pick up a corpse), move a carried corpse

    \return a value regarding the change (mainly internal to Qt).
*/
QVariant Entity::itemChange(QGraphicsItem::GraphicsItemChange change, const QVariant &value)
{
    if (change == ItemPositionChange && scene()) {
        // If is infected, try to infect nearby Entities
        if(isInfected())
        {
            infectUpdate();
        }

        // If is not infected and alive, tries to pick up or drop a corpse
        if(!_isInfected && _alive)
        {
            mourn();

            // Check if near the cemetery
            if(!_knowCemeteryLocation)
            {
                float distCemetery = sqrt(pow(x()-Entity::_cemeteryX, 2)+pow(y()-Entity::_cemeteryY, 2));
                if(distCemetery < 20)
                {
                    _knowCemeteryLocation = true;
                }
            }
        }

        // If carries an Entity, move to the same direction as this Entity
        if(_carry)
        {
            // If the carried Entity is too far from the one that carries it, adjust its position
            float dist = sqrt(pow(x()-_carried->x(), 2)+pow(y()-_carried->y(), 2));
            if(dist > 20)
            {
                _carried->setX(x());
                _carried->setY(y());
            }
            _carried->carry(_speedX, _speedY);
        }

    }

    return QGraphicsItem::itemChange(change, value);
}

// Private methods

/*!
    \fn float Entity::calcAngle(float directionX, float directionY)

    \brief Calcultate the angle of the direction the Entity is heading to.

    \param speedX the speed of the Entity horizontally.
    \param speedY the speed of the Entity vertically.

    \return The angle of the direction the Entity is heading to.

    \sa QGraphicsItem::setRotation(qreal angle);
*/
float Entity::calcAngle(float directionX, float directionY)
{
    //atan2 calculate the angle in rad
    //multiplied by 180/PI gives us the corresponding degree
    // +135 is needed because the coordinate system is different in our window ( (0,0) is in the top left hand corner)
    return atan2(directionY, directionX) * 180/PI + 135;
}

/*!
    \fn void Entity::initWay()
    \brief Choose randomly a direction in X and Y for the entity.
           Is used to ensure the entity will not have a 0/0 velocity.

    \note Might be removed because the Entity can move naturally with wander or with other Entities.
*/
void Entity::initWay()
{
    // Choose randomly a direction
    int x =  qrand()%3-1;
    int y = qrand()%3-1;

    // Ensure the Entity doesn't have a null velocity
    while (x == 0 && y == 0)
    {
        int test = qrand()%2;
        if(test == 0)
        {
            x = qrand()%3-1;
        }
        else
        {
            y = qrand()%3-1;
        }
    }

    // Give a random speed to the Entity (limited by the maxSpeed constant defined in Entity.h)
    _speedX = x*(qrand()%maxSpeed+1);
    _speedY = y*(qrand()%maxSpeed+1);
}

/*!
    \fn void Entity::getFriends()
    \brief Browse all the Entities to get nearby ones (radius is friendRadius constant) and add them to the _friends List of the Entity.
           It allows the methods called by advance() to calculate the change to do in the Entity.
           Also spread the coordinates of the cemetery to nearby Entities.

    \note Might cause performance issues when there's a lot of Entities (don't really know how to improve)
    \sa advance(), behaviour(), knowCemeteryLocation(), knowCemeteryLocation(bool knowCemeteryLocation)
*/
void Entity::getFriends()
{
    for(QGraphicsItem * obj : scene()->items())
    {
        Entity * ent = static_cast<Entity *>(obj);

        if(ent!=this)
        {
            if(abs(ent->x()-x()) < friendRadius && abs(ent->y()-y()) < friendRadius)
            {
                // Entities are friend only when they're alive and well (ostracism is strong with triangles)
                if((!ent->isInfected()) && (ent->isAlive()))
                {
                    _friends.append(ent);
                }

                // Share cemetery location
                if(_knowCemeteryLocation && ent->isAlive() && !ent->isInfected())
                {
                    if(!ent->knowCemeteryLocation())
                    {
                        ent->knowCemeteryLocation(true);
                    }
                }
            }
        }
    }
}

/*!
    \fn void Entity::behaviour()
    \brief Method used to give a behaviour to the entity.
           It is the main method of the Entity class as it is from here that every method
           influancing what does the Entity are called.
           It is here you need to add the call of a new method meant for modify the swarm intelligence.

    \note After adding a method for calling by behaviour(), you should add a static attribute to activate/deactivate the method
          and modify MainWindow::keyPressEvent(QKeyEvent *event) to add a way to deactivate the method.
          It is not mandatory but helps a lot, thanks.

    \sa getAverageDir(), getCohesion(), getAvoidDir(), wander(), limit(), blink(), MainWindow::keyPressEvent(QKeyEvent *event)
*/
void Entity::behaviour()
{
    // If need be, please go to according method documentation to understand what it does
    // to avoid having too much of the same explanations, thanks

    if((!_isInfected) || (_isInfected && !_careful))
    {
        if(_direction)
        {
            getAverageDir();
        }
        if(_cohesion)
        {
            getCohesion();
        }
    }

    if(_avoidFriends)
    {
        getAvoidDir();
    }

    if(_wander)
    {
        wander();
    }

    if(_careful)
    {
        avoidInfected();
    }

    limit();

    if(!isInfected() && _alive)
    {
        blink();
    }

}

/*!
    \fn void Entity::infectUpdate()
    \brief For each Entity in the infection area, infect with a certain probability

    \note As the probability to infect is hardcoded for now, if you want to have a desastrous pandemic,
          you need to reduce the according values in this method.
 */
void Entity::infectUpdate()
{
    for(QGraphicsItem * obj : scene()->items())
    {
        Entity * ent = static_cast<Entity *>(obj);

        if(ent->_alive)
        {
            // The more an Entity is close to this infected, the more it can be affected
            float dist = sqrt(pow(x()-ent->x(), 2)+pow(y()-ent->y(), 2));
            if(dist > 0 && dist < friendRadius/2)
            {
                if(qrand()%800 == 0)
                {
                    ent->infect(true);
                }
            }
            if(dist > 0 && dist < friendRadius/3)
            {
                if(qrand()%80 == 0)
                {
                    ent->infect(true);
                }
            }
            if(dist > 0 && dist < friendRadius)
            {
                if(qrand()%8000 == 0)
                {
                    ent->infect(true);
                }
            }
        }
    }
}

/*!
    \fn void Entity::mourn()
    \brief Method used by living Entities to gather their dead comrades.

    \sa behaviour(), drop(), carry(Entity * carried, int vx, int vy)
*/
void Entity::mourn()
{
    if(_carry)
    {
        if(_knowCemeteryLocation)
        {
            // direct the Entity towards the cemetery
            float sumX = Entity::_cemeteryX - x();
            float sumY = Entity::_cemeteryY - y();

            float mag = sqrt(sumX*sumX+sumY*sumY);
            _speedX += (sumX/mag);
            _speedY += (sumY/mag);
        }

        for(QGraphicsItem * obj : scene()->items())
        {
            Entity * ent = static_cast<Entity *>(obj);

            if(ent!=this)
            {
                // Check if a nearby Entity is in the cemetery to drop the carried Entity
                if(abs(ent->x()-x()) < friendRadius/4 && abs(ent->y()-y()) < friendRadius/4)
                {
                    if(ent->isInCemetery())
                    {
                        _carried->isInCemetery(true);
                        drop();
                        break;
                    }
                }
            }
        }
    }
    else
    {
        for(QGraphicsItem * obj : scene()->items())
        {
            Entity * ent = static_cast<Entity *>(obj);

            if(ent!=this)
            {
                if(abs(ent->x()-x()) < friendRadius/10 && abs(ent->y()-y()) < friendRadius/10)
                {
                    if(!ent->isAlive() && !ent->isCarried() && !ent->isInCemetery())
                    {
                        // Check if there is a corpse to carry
                        if(qrand()%2 == 0)
                        {
                            carry(ent, _speedX, _speedY);
                            break;
                        }
                    }
                }
            }
        }
    }
}

/*!
    \fn void Entity::carry(Entity * carried, int vx, int vy)
    \brief Affect an Entity to be carried and change its speed according to the Entity which carries

    \sa carried(float vx, float vy), drop()
*/
void Entity::carry(Entity * carried, int vx, int vy)
{
    _carry = true;
    _carried = carried;
    _carried->isCarried(true);
    _carried->carry(vx, vy);
}

/*!
    \fn void Entity::drop()
    \brief Drop the currently carried Entity.

    \sa carry(Entity * carried, int vx, int vy)
*/
void Entity::drop()
{
    // Change the color of the carried to white (cemetery color arbitrary choosen)
    if(_carried->isInCemetery())
    {
        _carried->setColor(Qt::white);
    }
    // Put the speed of the carried to 0 and remove the pointer to it
    _carried->carry(0, 0);
    _carried->isCarried(false);
    _carried = nullptr;
    _carry = false;
}

/*!
    \fn void Entity::getAverageDir()
    \brief Modify the speed of the Entity to match the average direction of nearby Entity.

    \sa behaviour()
*/
void Entity::getAverageDir()
{
    int friendCount = 0;
    float sumXDir = 0;
    float sumYDir = 0;

    for(Entity * ent : _friends)
    {
        if((!ent->isInfected()) && (ent->isAlive()))
        {
            float dist = sqrt(pow(x()-ent->x(), 2)+pow(y()-ent->y(), 2));
            if(dist > 0 && dist < friendRadius)
            {
                // Normalize the vectors and divides them by the distance before adding them to the total
                // Used to find the average direction of the group of friends

                float currentValX = ent->getSpeedX();
                float currentValY = ent->getSpeedY();
                float entSpeed = ent->magnitude();
                if(entSpeed != 0 && entSpeed != 1)
                {
                    currentValX /= entSpeed;
                    currentValY /= entSpeed;
                }
                // Almost impossible but in case
                if(dist != 0)
                {
                    currentValX /= dist;
                    currentValY /= dist;
                }

                sumXDir += currentValX;
                sumYDir += currentValY;

                // Not used here to smooth the movement
                // but might come handy
                friendCount++;
            }
        }
    }
    if(friendCount > 0)
    {
        _speedX += sumXDir;
        _speedY += sumYDir;
    }
}

/*!
    \fn void Entity::getCohesion()
    \brief Modify the speed of the Entity to match the average position of nearby Entity.

    \sa behaviour()
*/
void Entity::getCohesion()
{
    int friendCount = 0;
    float sumX = 0;
    float sumY = 0;

    for(Entity * ent : _friends)
    {
        if((!ent->isInfected()) && (ent->isAlive()))
        {
            // Find the average of the position of friends
            float dist = sqrt(pow(x()-ent->x(), 2)+pow(y()-ent->y(), 2));
            if(dist > 0 && dist < friendRadius)
            {
                sumX += ent->x();
                sumY += ent->y();

                friendCount++;
            }
        }
    }

    if(friendCount > 0)
    {
        // Calculate the vector needed to add to the current speed vector
        // to go towards the average position
        sumX = sumX/friendCount;
        sumY = sumY/friendCount;
        sumX = (sumX - x());
        sumY = (sumY - y());

        float mag = sqrt(sumX*sumX+sumY*sumY);
        sumX += (sumX/mag)*0.05;
        sumY += (sumY/mag)*0.05;

        // /40 needed to have a more fluid movement
        _speedX += sumX/40;
        _speedY += sumY/40;
    }
}

/*!
    \fn void Entity::getAvoidDir()
    \brief Modify the speed of the Entity to avoid nearby Entity.

    \sa behaviour()
*/
void Entity::getAvoidDir()
{
    int friendCount = 0;
    float sumXAvoid = 0;
    float sumYAvoid = 0;

    for(Entity * ent : _friends)
    {
        float dist = sqrt(pow(x()-ent->x(), 2)+pow(y()-ent->y(), 2));
        if(dist > 0 && dist < friendRadius/1.3)
        {
            // Create a vector to change direction if an Entity is too close
            float diffX = x() - ent->x();
            float diffY = y() - ent->y();

            float norm = sqrt(diffX*diffX+diffY*diffY);
            if(norm != 0 && norm != 1)
            {
                diffX = (diffX/norm);
                diffY = (diffY/norm);
            }
            // Almost impossible but in case
            if(dist != 0)
            {
                diffX /= dist;
                diffY /= dist;
            }

            sumXAvoid += diffX;
            sumYAvoid += diffY;

            friendCount++;
        }
    }

    if(friendCount > 0)
    {
        // /3 is a test, but it seems like the movement is fluid
        _speedX += sumXAvoid/3;
        _speedY += sumYAvoid/3;
    }
}

/*!
    \fn void Entity::wander()
    \brief Modify randomly the direction of the Entity.

    \sa behaviour()
 */
void Entity::wander()
{
    if(qrand()%100 >80)
    {
        // *0.1 to avoid wide movements
        _speedX += (qrand()%3-1)*0.1;
        _speedY += (qrand()%3-1)*0.1;
    }
}

/*!
    \fn void Entity::limit()

    \brief Limit the speed of the Entity to ensure it doesn't exceed the defined maxSpeed

    \note As it calls magnitude(), might cause some performance issues

    \sa magnitude(), behaviour()
*/
void Entity::limit()
{
    // If the magnitude of the speed vector of the Entity exceed maxSpeed^2
    // normalize the vector and multiply it by maxSpeed
    float mag = magnitude();
    float maxSpeedCond = isInfected() ? maxSpeed/2 : maxSpeed;
    if(mag*mag > maxSpeedCond*maxSpeedCond)
    {
        _speedX = (_speedX/mag)*maxSpeedCond;
        _speedY = (_speedY/mag)*maxSpeedCond;
    }
}

/*!
    \fn void Entity::blink()

    \brief Modify the blinktime of the Entity to match the average blink time of friends.
           Also modify the color to have a consistent behaviour.

    \note Works oddly, Entities do blink, but it seems like something's not happening as intended.

    \sa resetBrush(), behaviour()
*/
void Entity::blink() {

    int friendCount = 0;
    int countFriendColorBlue = 0;
    int countFriendColorRed = 0;
    float blinkValueNeighbors = 0.0;

    //Get all friends and count the number of blue and red ones
    for(Entity * ent : _friends) {
        float dist = sqrt(pow(x()-ent->x(), 2)+pow(y()-ent->y(), 2));
        if((dist > 0 && dist < friendRadius/2) && (!ent->isInfected()) && (ent->isAlive()))
        {
            if (ent->_color == Qt::red) {
                countFriendColorRed++;
            }
            else {
                countFriendColorBlue++;
            }
            //Make the sum of their blink time
            blinkValueNeighbors += ent->_blinktime;
            friendCount++;
       }
    }

    //Doing mean on the blinking time sum of friends
    if(friendCount > 0)
    {
        // Get the average color of friends and change self color to match it
        if (countFriendColorRed >= countFriendColorBlue && this->_color == Qt::blue) {
            this->_color = Qt::red;
        }
        else if (countFriendColorRed < countFriendColorBlue && this->_color == Qt::red) {
            this->_color = Qt::blue;
        }
         blinkValueNeighbors = blinkValueNeighbors/friendCount;
    }

    // Reset the timer with average blink time of neighbours
    _blinktime = blinkValueNeighbors;
    _timerBrush->setInterval(_blinktime);
}

/*!
    \fn void Entity::avoidInfected()
    \brief Modify the direction of the Entity to avoid infected Entities in the area
*/
void Entity::avoidInfected()
{
    int friendCount = 0;
    float sumXAvoid = 0;
    float sumYAvoid = 0;

    for(QGraphicsItem * obj : scene()->items())
    {
        Entity * ent = static_cast<Entity *>(obj);

        if(ent->isInfected())
        {
            // Works almost as getAvoidDir but influence more the speed of the Entity
            float dist = sqrt(pow(x()-ent->x(), 2)+pow(y()-ent->y(), 2));
            if(dist > 0 && dist < friendRadius/1.3)
            {
                float diffX = x() - ent->x();
                float diffY = y() - ent->y();

                float norm = sqrt(diffX*diffX+diffY*diffY);
                if(norm != 0 && norm != 1)
                {
                    diffX = (diffX/norm);
                    diffY = (diffY/norm);
                }
                // Almost impossible but in case
                if(dist != 0)
                {
                    diffX /= dist;
                    diffY /= dist;
                }

                sumXAvoid += diffX;
                sumYAvoid += diffY;

                friendCount++;
            }
        }
    }

    if(friendCount > 0)
    {
        // 10000 to avoid from afar
        _speedX += sumXAvoid*10000;
        _speedY += sumYAvoid*10000;
    }
}

/*!
    \fn void Entity::checkCemetery()
    \brief Method used to create the cemetery when the first Entity die.
           Set the cemetery position.

    \sa isInCemetery()
*/
void Entity::checkCemetery()
{
    _isInCemetery = true;
    Entity::_cemetery = true;
    _color = Qt::white;
    Entity::_cemeteryX = x();
    Entity::_cemeteryY = y();
}

// Private Slots
/*!
    \fn void Entity::resetBrush()
    \brief Slot called each x milliseconds where x is the drawingTime constant defined in entity.h.
           It changes the color of the Entity.

    \sa behaviour()
*/
void Entity::resetBrush() {
    if(_color == Qt::blue)
    {
        _color = Qt::red;
    }
    else
    {
        _color = Qt::blue;
    }
}

/*!
    \fn void Entity::continuation()
    \brief Slot called after x milliseconds to decide wether or not the Entity dies

    \sa infect()
 */
void Entity::continuation()
{
    // Dies
    if(qrand()%100+_risky > 100)
    {
        _speedX = 0;
        _speedY = 0;
        // Black is arbitrary choosen for dead Entities
        _color = Qt::black;

        // Not alive anymore
        _alive = false;

        // Don't take into account infection after death (not to mess with the cemetery mechanics)
        // Might change if we think it could be funny
        _isInfected = false;

        // Dead men tale no tales, and know nothing
        _knowCemeteryLocation = false;
    }
    else
    {
        // Start blinking again when recover
        infect(false);
        _timerBrush->start(_blinktime);
    }
}
